
import React from 'react'

import AuthForm from '../authform/authform'

class App extends React.Component {

    render() {
        return (
            <AuthForm/>
        );
    }
}

export default App;